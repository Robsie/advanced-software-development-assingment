#include "BinaryImage.h"
//CONSTRUCTORS
BinaryImage::BinaryImage(int r, int c, double* d, double thresh) : Matrix(r,c,d)
{
	_threshold = thresh;
	filter();
}

BinaryImage::BinaryImage(int r, int c,int thresh) : Matrix(r,c)
{
	_threshold = thresh;
	filter();
}
BinaryImage::BinaryImage(int r, int c) : Matrix(r,c)
{
	_threshold = 200.5;
	filter();
}

BinaryImage::BinaryImage()
{
	_data = new double[10];
	_rows = 5;
	_columns = 2;
	_threshold = 200.5;
}

BinaryImage::BinaryImage(const BinaryImage& binImage) // Copy constructor
{
	_rows = binImage.rows();
	_columns = binImage.columns();
	_data = new double[binImage.rows()*binImage.columns()];
	_threshold = binImage.threshold();
	for(int i = 0;i<_rows;i++)
	{
		for(int j =0;j<_columns;j++)
		{
			setItem(i,j,binImage.getItem(i,j));
		}
	}
}

BinaryImage::~BinaryImage() 
{
	//Nothing extra to delete, the Matrix deconstructor already deals with the _data deletion
}

//METHODS
void BinaryImage::setItem(int r, int c, double value) // set items, can only be 255 or 0
{
	//_data[getIndex(r,c)] = value;
	if(value >= _threshold)
	{
		_data[getIndex(r,c)] = 255.0;
	}
	else
	{
		//cout<<value<<endl;
		_data[getIndex(r,c)] = value;
	}
}

double BinaryImage::threshold() const
{
	return _threshold;
}

void BinaryImage::filter()
{
	for(int i = 0; i <_rows*_columns;i++)
	{
		if(_data[i] >= _threshold)
		{
			_data[i] = 255;
		}
		else
		{
			_data[i] = 0;
		}
	}
}

BinaryImage BinaryImage::getBlock(int r, int c ,int n)
{
	return getBlock(r,c,n,n);
}

BinaryImage BinaryImage::getBlock(int r, int c, int nR, int nC)
{
	BinaryImage block = BinaryImage(nR,nC,200);	

	for(int i = 0; i<nR;i++)
	{
		for(int j = 0; j<nC;j++) 
		{
			block.setItem(i,j,getItem(i+r,j+c));
		}
	}
	return block;
}


void BinaryImage::insertBlock(BinaryImage& binImage,int r,int c,int n)
{
	insertBlock(binImage,r,c,n,n);
}

void BinaryImage::insertBlock(BinaryImage& binImage,int r,int c,int nR,int nC)
{
	for(int i = 0; i < nR; i++)
	{
		for(int j = 0; j < nC; j++)
		{
			setItem(r+i, c+j,binImage.getItem(i,j));
		}
	}
}

//OPERATORS
BinaryImage BinaryImage::operator-(const BinaryImage& m)
{
	BinaryImage difference(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			difference.setItem(i,j, (getItem(i,j) - m.getItem(i,j)) );
		}
	}
	return difference;
}

BinaryImage BinaryImage::operator+(const BinaryImage& m)
{
	BinaryImage added(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			added.setItem(i,j, (getItem(i,j) + m.getItem(i,j)) );
		}
	}
	return added;
}


BinaryImage BinaryImage::operator*(const BinaryImage& m)
{
	BinaryImage multiplied(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			
			multiplied.setItem(i,j, (getItem(i,j) * m.getItem(i,j)) );
			
		}
	}
	return multiplied;
}

BinaryImage BinaryImage::operator/(const BinaryImage& m)
{
	BinaryImage divided(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			divided.setItem(i,j, (getItem(i,j) / m.getItem(i,j)) );
		}
	}
	return divided;
}

BinaryImage& BinaryImage::operator=(const BinaryImage& binImage)
{
	cout<<"equals operator bin image"<<endl;
	if (this == &binImage)
	{
		return *this;
	}
	else
	{
		delete [] _data;
		int r = binImage.rows();
		int c = binImage.columns();
		_data = new double[r*c];
		_threshold = binImage.threshold();
		for(int i = 0; i < r;i++)
		{
			for(int j = 0; j<c;j++)
			{
				setItem(i,j,binImage.getItem(i,j));
			}
		}
	return *this;
	}
}

