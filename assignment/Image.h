#ifndef IMAGE_H
#define	IMAGE_H
#include "Matrix.h"
class Image : public Matrix
{
private:
public:
	Image(int r, int c, double* d);
	Image(int r, int c);
	void drawRectangle(int,int,int,int);
	
};


#endif
