#include "GreyscaleImage.h"

//PRIVATE


//PUBLIC

//CONSTRUCTORS
GreyscaleImage::GreyscaleImage(int r, int c, double* d, double maxgrey) : Matrix(r,c,d)
{
	_maxGrey = maxgrey;
}

GreyscaleImage::GreyscaleImage()
{
	_data = new double[10];
	_maxGrey = 255;
	_rows = 5;
	_columns = 2;
}

GreyscaleImage::GreyscaleImage(int r, int c, double* d) : Matrix(r,c,d)
{
	_maxGrey = 255;
}

GreyscaleImage::GreyscaleImage(int r, int c) : Matrix(r,c)
{
	_maxGrey = 255;
}


GreyscaleImage::GreyscaleImage(const GreyscaleImage& Gs)
{
	_rows = Gs.rows();
	_columns = Gs.columns();
	_data = new double[Gs.rows()*Gs.columns()];
	_maxGrey = Gs.maxGrey();
	for(int i = 0;i<_rows;i++)
	{
		for(int j =0;j<_columns;j++)
		{
			//cout<<(i,j)<<endl;
			setItem(i,j,Gs.getItem(i,j));
		}
	}
}

GreyscaleImage::~GreyscaleImage() 
{
	//Nothing extra to delete, the Matrix deconstructor already deals with the _data deletion
}


double GreyscaleImage::maxGrey() const
{
	return _maxGrey;
}

void GreyscaleImage::setItem(int r, int c, double value)
{
	if (value < 0)
	{
		_data[getIndex(r,c)] = 0;
	}
	else if (value > _maxGrey)
	{
		_data[getIndex(r,c)] = _maxGrey;
	}
	else
	{
		_data[getIndex(r,c)] = value;
	}
}

void GreyscaleImage::filter()
{
	for(int i = 0; i < _rows * _columns; i++)
	{
		if(_data[i] > _maxGrey)
		{
			_data[i] = _maxGrey;
		}
	}
}

GreyscaleImage GreyscaleImage::getBlock(int r, int c, int n)
{
	return getBlock(r,c,n,n);
}

GreyscaleImage GreyscaleImage::getBlock(int r,int c,int nR,int nC)
{
	GreyscaleImage block = GreyscaleImage(nR,nC);	

	for(int i = 0; i<nR;i++)
	{
		for(int j = 0; j<nC;j++) 
		{
			block.setItem(i,j,getItem(i+r,j+c));
		}
	}
	return block;
}

void GreyscaleImage::insertBlock(GreyscaleImage& m,int r,int c, int n)
{
	insertBlock(m,r,c,n,n);
}

void GreyscaleImage::insertBlock(GreyscaleImage& m,int r,int c, int nR, int nC)
{
	for(int i = 0; i < nR; i++)
	{
		for(int j = 0; j < nC; j++)
		{
			setItem(r + i,c + j,m.getItem(i,j));
		}
	}
}

void GreyscaleImage::drawRect(int r, int c, int rn, int cn)
{
	for(int i = 0; i < rn; i ++)
	{
		for(int j = 0; j < cn; j ++)
		{
			if(j == 0 || j == cn-1 || i == 0 || i == rn-1)
			{
				setItem(i+r,j+c,0.0);
			}
			
		}
	}
}


//OPERATORS

GreyscaleImage GreyscaleImage::operator-(const GreyscaleImage& m)
{
	GreyscaleImage difference(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			difference.setItem(i,j, (getItem(i,j) - m.getItem(i,j)) );
		}
	}
	return difference;
}


GreyscaleImage GreyscaleImage::operator+(const GreyscaleImage& m)
{
	GreyscaleImage added(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			added.setItem(i,j, (getItem(i,j) + m.getItem(i,j)) );
		}
	}
	return added;
}


GreyscaleImage GreyscaleImage::operator*(const GreyscaleImage& m)
{
	GreyscaleImage multiplied(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			
			multiplied.setItem(i,j, (getItem(i,j) * m.getItem(i,j)) );
			
		}
	}
	return multiplied;
}

GreyscaleImage GreyscaleImage::operator/(const GreyscaleImage& m)
{
	GreyscaleImage divided(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			divided.setItem(i,j, (getItem(i,j) / m.getItem(i,j)) );
		}
	}
	return divided;
}

GreyscaleImage& GreyscaleImage::operator=(const GreyscaleImage& m) // Equals operator
{
	if (this == &m)
	{
		return *this;
	}
	else
	{
		delete [] _data;
		int r = m.rows();
		int c = m.columns();
		_data = new double[r*c];
		_maxGrey = m.maxGrey();
		for(int i = 0; i < r;i++)
		{
			for(int j = 0; j<c;j++)
			{
				setItem(i,j,m.getItem(i,j));
			}
		}
	return *this;
	}
}