#ifndef IMAGEMANIPULATION_H
#define	IMAGEMANIPULATION_H
#include <string>
#include "GreyscaleImage.h"
#include "BinaryImage.h"
#include "Files.h"
#include <vector>
#include <time.h>

void unshuffle(string sourcefile, string targetfile);
BinaryImage unshuffle(BinaryImage&,BinaryImage&,int);

BinaryImage getClosestMatch(BinaryImage&,BinaryImage&, int n);
BinaryImage getClosestMatch(BinaryImage&,BinaryImage&, int Rn, int Cn);

void findWally(string sourcefile, string targetfile);
GreyscaleImage findWally(GreyscaleImage&,GreyscaleImage&,int,int,int);
bool isWithinTolerance(int r_Current, int c_Current, int r_Candidate, int c_Candidate,int r_Position, int c_Position,int tolerance);

#endif