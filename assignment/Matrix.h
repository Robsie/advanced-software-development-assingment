#ifndef MATRIX_H
#define	MATRIX_H
#include <iostream>
using namespace std;
class Matrix
{
protected:
	int _rows;
	int _columns;
	double* _data;
public:
	//constructors
	Matrix();
	Matrix(int,int,double*);
	Matrix(int,int);
	Matrix(const Matrix&);
	~Matrix();

	//Functions
	int rows() const; //returns the row number
	int columns() const; //returns the column number
	int getIndex(int, int) const; //returns the index of an item in the one dimensional array, as if it were 2 dimensional

	//legacy operator operations
	Matrix add(int,int, Matrix&); 
	Matrix minus(int,int,Matrix&);
	Matrix multiply(int,int,Matrix&);
	Matrix divide(int,int,Matrix&);

    Matrix getBlock(int, int, int, int); //Gets a rectangular block from the matrix, returns by value
	Matrix getBlock(int, int, int); // gets a square block from the matrix, returns by value

    void insertBlock(Matrix&,int,int,int); // Inserts a square block into the matrix
	void insertBlock(Matrix&,int,int,int,int); // Inserts a rectangular block into the matrix

    double getItem(int, int) const; // Get item from matrix by value
	virtual void setItem(int,int,double); // set item 

	//both use long double incase a very large image is used, which may exceed the size of a double
	long double sum(); // returns sum of the matrix
	long double squaredSum(); // returns squared sum of the matrix

	void highPassFilter();
	void filterWhite();

	//operators
	double& operator[] (int i);
	double& operator() (int r, int c);

	Matrix operator+(const Matrix&);
    Matrix operator-(const Matrix&);
	Matrix operator*(const Matrix&);
	Matrix operator/(const Matrix&);

	virtual Matrix& operator=(const Matrix& ); //Equals operator, virtual function can be overidden

	operator double*() const; //conversion operator
};
#endif