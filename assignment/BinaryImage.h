#ifndef BINARYIMAGE_H
#define	BINARYIMAGE_H
#include "Matrix.h"
class BinaryImage : public Matrix
{
private:
	//Matrix::drawRect(int r, int c, int rn, int cn);
protected:
	double _threshold;
public:
	BinaryImage();
	BinaryImage(int r, int c, double* d, double thresh);
	BinaryImage(int r, int c, int thresh);
	BinaryImage(int r, int c);
	BinaryImage(const BinaryImage&);
	~BinaryImage();
	double threshold() const;

	BinaryImage getBlock(int, int ,int);
	BinaryImage getBlock(int, int, int,int);

    virtual void setItem(int,int,double);
	void insertBlock(BinaryImage&,int,int,int);
	void insertBlock(BinaryImage&,int,int,int,int);
	virtual void filter();

	BinaryImage operator-(const BinaryImage&); // minus operator
	BinaryImage operator+(const BinaryImage&); // add operator
	BinaryImage operator*(const BinaryImage&); // multiply operator
	BinaryImage operator/(const BinaryImage&); // divide operator

	BinaryImage& operator=(const BinaryImage& );
};


#endif
