#ifndef GREYSCALEIMAGE_H //Header guards
#define	GREYSCALEIMAGE_H

#include "Matrix.h"

class GreyscaleImage : public Matrix
{
protected:
	double _maxGrey; //Stores the maxiumum grey value 
public:
	//CONSTRUCTORS
	GreyscaleImage(); //default constructor, generally should remain un-used
	GreyscaleImage(int r, int c, double* d, double maxgrey); //constructor with all values set
	GreyscaleImage(int r, int c, double* d);//constructor without max grey
	GreyscaleImage(int r, int c); //constructor without data being provided, will all be set to 0's
	GreyscaleImage(const GreyscaleImage&); //copy constuctor
	~GreyscaleImage(); //Destructor

	//METHODS
	virtual void setItem(int,int,double); //set item, virtual function
	void drawRect(int r, int c, int rn, int cn); //draws a rectangle
	double maxGrey() const; //returns max grey value
	GreyscaleImage getBlock(int,int,int); //returns a block by value with 3 in parameters, calls the 4 parameter equivelent
	GreyscaleImage getBlock(int,int,int,int); //returns a block by value with 4 int paramaters
	virtual void filter(); // filters the data, to make sure none exceed the maximum grey value
	void insertBlock(GreyscaleImage&,int,int,int); //inserts a block into the array with 3 parameters, calls the 4 parameter equivelent
	void insertBlock(GreyscaleImage&,int,int,int,int); //inserts a block into the array with 4 parameters

	//Operators
	GreyscaleImage operator-(const GreyscaleImage&); // minus operator
	GreyscaleImage operator+(const GreyscaleImage&); // add operator
	GreyscaleImage operator*(const GreyscaleImage&); // multiply operator
	GreyscaleImage operator/(const GreyscaleImage&); // divide operator

	virtual GreyscaleImage& operator=(const GreyscaleImage&); //Equals operator

};



#endif
