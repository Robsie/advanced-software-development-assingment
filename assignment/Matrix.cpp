#include "Matrix.h"
//#include <iostream>

//PRIVATE


Matrix::Matrix(int r, int c,double* d)
{
	_data = new double[r*c];
	//load _data into array
	for(int i =0; i<(c*r); i++)
	{
		_data[i] = d[i];
	}
	_rows = r;
	_columns = c;
}
Matrix::Matrix()
{
	_data = new double[10];
}

Matrix::Matrix(int r, int c)
{
	_rows = r;
	_columns = c;
	_data = new double[r*c];
	for(int i =0; i<(c*r); i++)
	{
		_data[i] = 0;
	}
}

Matrix::Matrix(const Matrix& m)
{
	cout<<"copy constructor matrix"<<endl;
	_rows = m.rows();
	_columns = m.columns();
	_data = new double[_rows*_columns];
	for(int i = 0;i<_rows;i++)
	{
		for(int j =0;j<_columns;j++)
		{
			//cout<<(i,j)<<endl;
			setItem(i,j,m.getItem(i,j));
		}
	}
}

Matrix::~Matrix()
{
	delete [] _data;
}

/*
	PUBLIC
*/
int Matrix::getIndex(int x, int y) const
{
	//cout<<(y + (x * _columns))<<endl;
	return(x + (y * _rows));
}

double Matrix::getItem(int r, int c) const
{
	return _data[getIndex(r,c)];
}

void Matrix::setItem(int r, int c, double value)
{
	_data[getIndex(r,c)] = value;
}

int Matrix::rows() const
{
	return _rows;
}

int Matrix::columns() const
{
	return _columns;
}


Matrix Matrix::getBlock(int r,int c,int nR,int nC)
{
//	cout<<"get Block r c nr nc "<<endl;
	Matrix block = Matrix(nR,nC);
	//cout<<block.columns()<<endl;
	//cout<<nR<<" "<<nC<<endl;
	for(int i = 0; i<nR;i++)
	{
		for(int j = 0; j<nC;j++) 
		{
			block.setItem(i,j,getItem(i+r,j+c));
			//cout<<i<<" "<<j<<endl;
		}
	}
	//cout<<"returning block"<<endl;
	return block;
}

Matrix Matrix::getBlock(int r,int c,int blocksize)
{
	//cout<<"get Block r, c, blocksize "<<endl;
	return getBlock(r,c,blocksize,blocksize);
}

void Matrix::insertBlock(Matrix& m,int r,int c, int blocksize)
{
	for(int i = 0; i < blocksize; i++)
	{
		for(int j = 0; j < blocksize; j++)
		{
			setItem(r + i,c + j,m.getItem(i,j));
		}
	}
}

void Matrix::insertBlock(Matrix& m,int r,int c, int nR, int nC)
{
	for(int i = 0; i < nR; i++)
	{
		for(int j = 0; j < nC; j++)
		{
			setItem(r + i,c + j,m.getItem(i,j));
		}
	}
}

long double Matrix::sum()
{
	long double sum = 0;
	for(int i = 0; i<= _rows * _columns;i++)
	{
		sum += _data[i];
	}
	return sum;
}

long double Matrix::squaredSum()
{
	long double sqsum = 0;
	for(int i = 0; i<= _rows * _columns;i++)
	{

		sqsum += pow(_data[i],2);
	}
	return sqsum;
}

void Matrix::highPassFilter()
{
	for(int i = 0; i < (rows() * columns()); i++)
	{
		if(_data[i] < 200)
		{
			_data[i] = 0;
		}
	}
}

void Matrix::filterWhite()
{
	for(int i =0; i<_rows*_columns; i++)
	{
		if(_data[i] > 230)
		{
			_data[i] = 125;
		}
	}
}


/////Operators//////

Matrix Matrix::operator+(const Matrix& m)
{
	Matrix added(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			added.setItem(i,j, (getItem(i,j) + m.getItem(i,j)) );
		}
	}
	return added;
}

Matrix Matrix::operator-(const Matrix& m)
{
	Matrix difference(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			difference.setItem(i,j, (getItem(i,j) - m.getItem(i,j)) );
		}
	}
	return difference;
}

Matrix Matrix::operator*(const Matrix& m)
{
	Matrix multiplied(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			
			multiplied.setItem(i,j, (getItem(i,j) * m.getItem(i,j)) );
			
		}
	}
	return multiplied;
}

Matrix Matrix::operator/(const Matrix& m)
{
	Matrix divided(_rows,_columns);
	for(int i = 0; i < _rows; i++)
	{
		for(int j = 0; j < _columns; j++)
		{
			divided.setItem(i,j, (getItem(i,j) / m.getItem(i,j)) );
		}
	}
	return divided;
}

Matrix& Matrix::operator=(const Matrix& m)
{
	cout<<"equals operator matrix"<<endl;
	if (this == &m)
	{
		return *this;
	}
	else
	{
		delete [] _data;
		int r = m.rows();
		int c = m.columns();
		_data = new double[r*c];
		for(int i = 0; i < r;i++)
		{
			for(int j = 0; j<c;j++)
			{
				setItem(i,j,m.getItem(i,j));
			}
		}
	return *this;
	}
}

double& Matrix::operator[](int i)
{
	return _data[i];
}

double& Matrix::operator() (int r, int c) //returns a reference to the item at _data[r][c]
{
	return(_data[getIndex(r,c)]);
}

Matrix::operator double*() const
{
	return _data;
}