#include "ImageManipulation.h"


//This is the testing function for task 1, both images need to be 512 by 512
void unshuffle(string sourcefile, string targetfile)
{
	cout<<"Testing image unshuffling code \n"<<endl;
	double* tempData = LoadTextFile(sourcefile,512,512);
	BinaryImage sourceImage(512,512,tempData,200);
	delete [] tempData;
	OutputPGMImage("source.pgm",sourceImage,512,512,255);
	tempData = LoadTextFile(targetfile,512,512);
	BinaryImage targetImage(512,512,tempData,200);
	delete [] tempData;
	OutputPGMImage("target.pgm",targetImage,512,512,255);

	clock_t startTime = clock();
	BinaryImage unshuffled = unshuffle(targetImage,sourceImage,16);
	cout << double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds. \n" << endl;
	OutputPGMImage("unshuffled.pgm",unshuffled,512,512,255);
}

//This is the testing code for task 2, it only takes the file names, image one needs to be 1024*768 and the second 36*46
void findWally(string sourcefile,string targetfile)
{
	cout<<"Testing find wally code: \n"<<endl;
	double* tempData;
	tempData = LoadTextFile(sourcefile,1024,768);
	GreyscaleImage wallyscene(1024,768,tempData,255);
	delete [] tempData;
	tempData = LoadTextFile(targetfile,36,46);
	GreyscaleImage wally(36,46,tempData,255);
	delete [] tempData;
	
	clock_t startTime = clock();
	OutputPGMImage("foundhim.pgm",findWally(wally,wallyscene,2,23,2),1024,768,255);
	cout << double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
	
}



BinaryImage unshuffle(BinaryImage& target, BinaryImage& shuffled, int n)
{
	BinaryImage output(512,512);
	//target.highPassFilter();
	for(int i =0; i<=512 - n; i+=n)
	{
		for(int j = 0; j<=512 - n; j+=n)
		{
			BinaryImage targetTemp = target.getBlock(i,j,n,n);
			BinaryImage bestMatch = getClosestMatch(shuffled,targetTemp,n);
			output.insertBlock(bestMatch,i,j,n);	
		}
	}
	return output;
}

BinaryImage getClosestMatch(BinaryImage& source, BinaryImage& target, int n)
{
	return(getClosestMatch(source,target,n,n));
}

BinaryImage getClosestMatch(BinaryImage& source, BinaryImage& target, int Rn, int Cn)
{
		long double bestMatch = 99999999;
		int closestRIndex = 0;
		int closestCIndex = 0;
		for(int k =0; k<source.rows(); k+=Rn)
		{
			for(int l = 0; l<source.columns(); l+=Cn)
			{
				BinaryImage sourceTemp = source.getBlock(k,l,Rn,Cn);
				BinaryImage difference = sourceTemp - target;
				long double squaresum = difference.squaredSum();
				if(squaresum < bestMatch)
				{
					bestMatch = squaresum;
					closestRIndex = k;
					closestCIndex = l;
					if(bestMatch <=1)
					{
						return source.getBlock(closestRIndex,closestCIndex,Rn,Cn);
					}							
				}
			}
		}
		return source.getBlock(closestRIndex,closestCIndex,Rn,Cn);
}


GreyscaleImage findWally(GreyscaleImage& target, GreyscaleImage& source, int blockR, int blockC,int increment)
{
	struct positions
	{
		int r;
		int c;
		int n;
	};
	//setting as local variables for performance reasons, somehow this cuts 10 seconds off runtime
	int sourceRows = source.rows();
	int sourceColumns = source.columns();
	int targetRows = target.rows();
	int targetColumns = target.columns();

	vector<positions> bestGuesses; //declares a vector to hold wally best guess positions
	GreyscaleImage output = source; // sets the output to the source image
	//for each block of the target image
	for(int i =0; i<targetRows; i+=blockR) 
	{
		for(int j = 0; j<targetColumns; j+=blockC)
		{
			GreyscaleImage targetTemp = target.getBlock(i,j,blockR,blockC); //temporary image to hold 
			long double bestMatchIndex = 9999999999999;
			int closestRIndex = 0;
			int closestCIndex = 0;
			//for each block of the source image
			for(int k =0; k<sourceRows; k+=increment)
			{
				for(int l = 0; l<sourceColumns; l+=increment)
				{
					if(k<sourceRows-blockR && l<sourceColumns-blockC) // ensures no out of bound values
					{
						GreyscaleImage sceneTemp = source.getBlock(k,l,blockR,blockC);
						GreyscaleImage difference = sceneTemp - targetTemp;
						long double squareSum = difference.squaredSum();
						if(squareSum < bestMatchIndex) // If its a better match than the current best match
						{
							bestMatchIndex = squareSum;
							closestRIndex = k;
							closestCIndex = l;
						}
					}	
				}
			}	
		
			bool found = false;
			for(size_t x = 0; x < bestGuesses.size(); x++)
			{	
				//cout<<bestGuesses[x].r<<" "<<closestRIndex - i<<endl;
				
				if(isWithinTolerance(bestGuesses[x].r,bestGuesses[x].c,closestRIndex,closestCIndex,i,j,2)) // if it matches
				{
					bestGuesses[x].n +=1;  // increment the instances found by 1
					found = true; 
					break;
				}
			}
			if (found == false || bestGuesses.size() == 0 ) // if item not already in the vector
			{
				positions newItem; // create a new structure
				newItem.r = closestRIndex - i;
				newItem.c = closestCIndex - j;
				newItem.n = 1;
				bestGuesses.push_back(newItem); // inserts the new item into the back of the vector
			}
		}
	}
	int highestIndex = 0; //Index of the best fit block
	int mostItems = 0; //keeps track of the block with the highest instances
	for(size_t x = 0; x < bestGuesses.size() - 1; x++) // for each item in the vector
	{
		if (bestGuesses[x].n > mostItems) 
		{
			mostItems = bestGuesses[x].n;
			highestIndex = x;
		}
	}

	if(mostItems < 2) //if there is no match above 1
	{
		cout<<"wally has not been found, no match good enough"<<endl;
	}
	else 
	{
		OutputPGMImage("wally_found.pgm", source.getBlock(bestGuesses[highestIndex].r, bestGuesses[highestIndex].c,targetRows,targetColumns), targetRows,targetColumns,255); //output the image that it thinks is wally
		output.drawRect(bestGuesses[highestIndex].r,bestGuesses[highestIndex].c, targetRows, targetColumns); //draw a rectangle around wally onto the output image
	}
	//These lines are only for testing purpouses
	cout<<"vector size: "<<bestGuesses.size()<<endl;
	cout<<"Most instances: "<<mostItems<<endl;
	cout<<"Location: "<< bestGuesses[highestIndex].r << "   "<< bestGuesses[highestIndex].c<<endl;

	return output;
}

bool isWithinTolerance(int r_Current, int c_Current, int r_Candidate, int c_Candidate,int r_Position, int c_Position,int tolerance)
{
	if(( r_Current >= r_Candidate - r_Position - tolerance && r_Current <= r_Candidate - r_Position + tolerance)   //if its within the accepted range
		&& (c_Current >= c_Candidate - c_Position - tolerance && c_Current <= c_Candidate - c_Position + tolerance))
	{
		return true;
	}
	else
	{
		return false;
	}
}