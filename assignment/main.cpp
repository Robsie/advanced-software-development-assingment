#include "ImageManipulation.h"
using namespace std;

int main()
{
	unshuffle("logo_shuffled.txt","logo_with_noise.txt");
	findWally("cluttered_scene.txt","wally_grey.txt");
	system("pause");

	return 0;
}


































//Old code

//Matrix unshuffle(Matrix& target, Matrix& shuffled, int n)
//{
//	Matrix output(512,512);
//	target.highPassFilter();
//	for(int i =0; i<=512 - n; i+=n)
//	{
//		for(int j = 0; j<=512 - n; j+=n)
//		{
//			Matrix targetTemp = target.getBlock(i,j,n,n);
//			Matrix bestMatch = getClosestMatch(shuffled,targetTemp,n);
//			output.insertBlock(bestMatch,i,j,n);	
//		}
//	}
//	return output;
//}
//
//Matrix getClosestMatch(Matrix& source, Matrix& target, int n)
//{
//		long double bestMatch = 99999999;
//		int closestRIndex = 0;
//		int closestCIndex = 0;
//		for(int k =0; k<source.rows(); k+=n)
//		{
//			for(int l = 0; l<source.columns(); l+=n)
//			{
//				Matrix sourceTemp = source.getBlock(k,l,n);
//				Matrix difference = sourceTemp - target;
//				long double squaresum = difference.squaredSum();
//				if(squaresum < bestMatch)
//				{
//					bestMatch = squaresum;
//					closestRIndex = k;
//					closestCIndex = l;
//					if(bestMatch < 6400)
//					{
//						return source.getBlock(closestRIndex,closestCIndex,n);
//					}							
//				}
//			}
//		}
//		return source.getBlock(closestRIndex,closestCIndex,n);
//}
//
//Matrix findWally(Matrix& target, Matrix& source, int n)
//{
//	cout<<"finding wally"<<endl;
//	clock_t startTime = clock();
//	struct positions
//	{
//		int r;
//		int c;
//		int n;
//	};
//
//	vector<positions> bestBlocks;
//	Matrix output = source;
//	for(int i =0; i<36; i+=4)
//	{
//		for(int j = 0; j<46; j+=23)
//		{
//			cout<<i<<" "<<j<<endl;
//			Matrix targetTemp = target.getBlock(i,j,4,23);
//			long double bestMatchIndex = 9999999999999;
//	//		//Matrix bestMatch(2,2,0);
//			int closestRIndex = 0;
//			int closestCIndex = 0;
//			
//			for(int k =0; k<1024; k+=2)
//			{
//				for(int l = 0; l<768; l+=1)
//				{
//					if(k<1024-4 && l<768-23)
//					{
//						//cout<<k<<" "<<l<<endl;
//						Matrix sceneTemp = source.getBlock(k,l,4,23);
//						Matrix difference = sceneTemp - targetTemp;
//						long double squareSum = difference.squaredSum();
//						//cout<<squaresum<<endl;
//						if(squareSum < bestMatchIndex)
//						{
//							//cout<<"bestMatch"<<endl;
//							bestMatchIndex = squareSum;
//							closestRIndex = k;
//							closestCIndex = l;
//							//system("pause");
//						}
//					}	
//				}
//			}	
//		
//			bool found = false;
//			for(size_t x = 0; x < bestBlocks.size(); x++)
//			{	
//			//	cout<<bestBlocks[x].r<<" "<<closestRIndex - i<<endl;
//				
//				if(bestBlocks[x].r == closestRIndex - i && bestBlocks[x].c == closestCIndex - j)
//				{
//					bestBlocks[x].n +=1;
//				//	cout<<bestBlocks[x].r<<endl;
//					found = true;
//					break;
//				}
//			}
//			if (found == false || bestBlocks.size() == 0 )
//			{
//				positions newItem;
//				newItem.r = closestRIndex - i;
//				newItem.c = closestCIndex - j;
//				newItem.n = 1;
//				bestBlocks.push_back(newItem);
//			}
//	
//		}
//	}
//	int highestIndex = 0;
//	int mostItems = 0;
//	for(size_t x = 0; x < bestBlocks.size() - 1; x++)
//	{
//		if (bestBlocks[x].n > mostItems)
//		{
//			mostItems = bestBlocks[x].n;
//			highestIndex = x;
//		}
//	}
//	cout<<"outputting"<<endl;
//	//output.insertBlock(source.getBlock(bestBlocks[highestIndex].r,bestBlocks[highestIndex].c,36,46),bestBlocks[highestIndex].r,bestBlocks[highestIndex].c,36,46);
//	output.drawRect(bestBlocks[highestIndex].r,bestBlocks[highestIndex].c, 36, 46);
//	//cout<<bestBlocks.size()<<endl;
//	cout << double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << endl;
//
//	return output;
//}
































