#ifndef FILES_H
#define	FILES_H

#include <sstream> 
#include <iostream>  
#include <fstream> 
#include <istream>
using namespace std;

void OutputPGMImage(string,double*,int,int,int);
double* LoadTextFile(string, int,int);

#endif